# Composer template for Drupal projects

Template for Drupal 8 Projects with Behat, Phing and PHPCS

## Usage

Clone the repo

Edit custom/build.properties.local file with:

```
drupal.db.name = [database name]
drupal.db.password = [database password]
drupal.base_url = http://[site base url]/
```
and other properties you might want to change.


To install:

```
composer install
```
